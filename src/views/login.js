import React, { Component } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withRouter } from "react-router-dom";


const useStyles = makeStyles(theme => ({
    margin: {
      margin: theme.spacing(1),
    },
  }));

//let history = useHistory();
  
class Login extends Component{    
    constructor(props){
        super(props);
        this.state = {
            email: "",
            password: "", 
            from: "/",
            
        };
        this.login = this.login.bind(this);    
    }
    

    login(){
        // mock login
        const { history } = this.props;
        setTimeout(() => {
            if(this.state.email === "admin@admin.com" && this.state.password === "password"){
                sessionStorage.setItem("loggedInUser", "admin@admin.com");
                history.replace(this.state.from);
                return;
            }
            
        }, 500);
    }

    render(){
        return(
            <div>
                LOGIN
                <Container maxWidth="md">
                    <FormControl >
                        <TextField
                            id="email"
                            label="Email"
                            type="email"
                            value={this.state.email}
                            onChange={(e) => this.setState({email: e.target.value})}
                            margin="normal"
                        />
                        <TextField
                            id="password"
                            label="Password"
                            value={this.state.password}
                            onChange={(e) => this.setState({password: e.target.value})}
                            type="password"
                            margin="normal"
                        />
                        <Button variant="contained" color="primary" onClick={this.login}>
                            LOGIN
                        </Button>
                    </FormControl>
                </Container> 
            </div>
        );
    }

}

export default withRouter(Login);