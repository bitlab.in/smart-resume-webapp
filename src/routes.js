import React, { Component } from "react";
import {Switch, Route, Redirect } from "react-router-dom";

import Login from "./views/login";
import Home from "./views/home";

export default class Routes extends Component{
    constructor(props){
        super(props);
        this.state = {
            loaded: false
        };

        this.checkLogin = this.checkLogin.bind(this);
    }

    checkLogin(props){
        // add check to store
        let userId = sessionStorage.getItem("loggedInUser");
        if(!userId){
            return false;
        }
        return true;
    }

    render(){
        const PrivateRoute = ({component: Component, ...rest }) => (
            <Route {...rest} render={props => (
                this.checkLogin(props) == true ? 
                <Component {...props} /> :
                <Redirect
                        to={{
                        pathname: "/login",
                        state: { from: props.location }
                        }}
                    />)
            } />
        );
        
        return(
            <div>
                <Switch>
                    <Route path="/login" component={Login}></Route>
                    <PrivateRoute path="/" component={Home}></PrivateRoute>

                </Switch>
            </div>
        );
    }

}