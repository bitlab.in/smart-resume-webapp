import React from 'react';
import logo from './logo.svg';
import './App.css';
import Routes from "./routes";

function App() {
  return (
    <div className="App" id="App">
      <Routes />
    </div>
  );
}

export default App;
